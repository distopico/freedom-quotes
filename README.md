# Freedom Quotes

Just another [fortune-mod](https://linux.die.net/man/6/fortune) cookies with quotes
about freedom and social movements in Spanish and potentially other languages.

All fortunes' text and quotes are presumed to be in the public domain. However
if any non-free fortune is present, please notify it to the package maintainer
to remove it.
